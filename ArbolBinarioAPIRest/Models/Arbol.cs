﻿using System;
using System.Linq;

namespace ArbolBinarioAPIRest.Models
{
    public class Arbol
    {
        private Nodo raiz;

        public Arbol()
        {
            raiz = null;
            Cantidad = 0;
        }

        public bool IsEmpty
        {
            get { return raiz == null; }
        }

        public void Insertar(int dato)
        {
            if (IsEmpty)
            {
                raiz = new Nodo(dato);
            }
            else
            {
                raiz.Insertar(raiz, dato);
            }

            Cantidad++;
        }

        public int[] Agregar(int[] ancestros)
        {
            int ancestroMasCercano = 0;
            foreach (var ancestro in ancestros)
            {
                ancestroMasCercano = raiz.BuscarAncestro(raiz, ancestro, raiz);
                if (!ancestros.Contains(ancestroMasCercano)) { 
                    //Se aumenta el tamaño de la lista para inserta un nuevo ancestro
                    Array.Resize(ref ancestros, ancestros.Length + 1);
                    ancestros.SetValue(ancestroMasCercano, ancestros.Length - 1);
                    //Si encuentra que el ancestro es la raiz del arbol finaliza el bucle, 
                    //sino actualizar la lista de ancestros del nodo ingresado correspondiente
                    if (ancestroMasCercano == raiz.numero)
                        break;
                    else ancestros = Agregar(ancestros);
                }
            }
            return ancestros;
        }

        public int Buscar(int busqueda1, int busqueda2)
        {
            int ancestroMasCercano = 0;
            int[] ancestros1 = { raiz.BuscarAncestro(raiz, busqueda1, raiz) };
            int[] ancestros2 = { raiz.BuscarAncestro(raiz, busqueda2, raiz) };
            //Se comparan ancestros de los nodos ingresados
            if (raiz.BuscarAncestro(raiz, busqueda1, raiz) != raiz.BuscarAncestro(raiz, busqueda2, raiz))
            {
                //Se arma lista de ancestros de los nodos ingresados
                ancestros1 = Agregar(ancestros1);
                ancestros2 = Agregar(ancestros2);
                foreach (var ancestro in ancestros1) {
                    if (ancestros2.Contains(ancestro))
                    {
                        //Se comparan ancestros en las listas hasta encontrar el primer nodo comun mas cercano
                        ancestroMasCercano = ancestro;
                        break;
                    }
                }
                                
            }
            else
                ancestroMasCercano = raiz.BuscarAncestro(raiz, busqueda1, raiz);
            return ancestroMasCercano;
        }
        
        public int Cantidad { get; private set; }
    }
}