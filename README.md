# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Dado	un	árbol	binario,	encuentre	el	ancestro	común	más	cercano	entre	dos	nodos.
Input	:	

* 70,84,85
* 70,84,78,80
* 70,84,78,76
* 70,49,54,51
* 70,49,37,40
* 70,49,37,22

El	árbol	correspondiente	a	estos	datos	es	el	siguiente:

             	     70
			49			       84
		37		54        78	   85
	 22  40   51 	    76  80

Output:

ancestor(40,78)	=	70

ancestor(51,37)	=	49

ancestor(76,85)	=	84

Diseñe	un	API REST que	permita :

1. Crear	un	árbol.

2. Dado	un	arbol	y	dos	nodos,	retorne	el	ancestro	común más	cercano.

### How do I get set up? ###

Descargar, abrir solucion, y correr en VS Community 2017 o superior.

Probar en postman o simular.

### Datos de entrada y URL

-Imagen 1 https://imgur.com/VTduhec

-Imagen 2 https://imgur.com/KjzL3uo

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact