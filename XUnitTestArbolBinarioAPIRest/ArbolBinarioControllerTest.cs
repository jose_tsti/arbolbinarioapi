using ArbolBinarioAPIRest.Controllers;
using System;
using Xunit;

namespace XUnitTestArbolBinarioAPIRest
{
    public class ArbolBinarioControllerTest
    {
        ArbolBinarioController _controller;

        public ArbolBinarioControllerTest()
        {
            _controller = new ArbolBinarioController();
        }

        [Fact]
        public void Get_WhenCalled_ReturnsOkResult()
        {
            // Act
            var okResult = _controller.GetAncestroCercano("70,84,85\n70,84,78,80\n70,84,78,76\n70,49,54,51\n70,49,37,40\n70,49,37,22", 40, 78);

            // Assert
            Assert.IsType<int>(okResult);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsValue()
        {
            // Act
            var okResult = _controller.GetAncestroCercano("70,84,85\n70,84,78,80\n70,84,78,76\n70,49,54,51\n70,49,37,40\n70,49,37,22", 40, 78);

            // Assert
            var item = Assert.IsType<int>(okResult);
            Assert.Equal(70, item);
        }
    }
}
